FROM debian:buster
RUN apt update && \
    apt install -y etcd && \
    apt clean
COPY docker-entrypoint.sh /
EXPOSE 2379 2380
CMD [ "/docker-entrypoint.sh" ]