# ETCD

[![Gitlab CI build status badge](https://gitlab.com/visualt-studio/etcd/badges/main/pipeline.svg)](https://gitlab.com/visualt-studio/etcd/-/commits/main)

_A distributed, reliable key-value store for the most critical data of a distributed system._

You can configure etcd through the following:

* [Command-line flags](https://etcd.io/docs/v3.5/op-guide/configuration/#command-line-flags)
* **Environment variables**: every flag has a corresponding environment variable that has the same name but is prefixed with `ETCD_` and formatted in all caps and snake case. For example, `--some-flag` would be `ETCD_SOME_FLAG`.
* [Configuration file](https://etcd.io/docs/v3.5/op-guide/configuration/#configuration-file)

The full list of etcd configurations can be found [here](https://etcd.io/docs/v3.5/op-guide/configuration/).
